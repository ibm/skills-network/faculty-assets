# Logos
To use these logos, please append the file name to the below base URL.
```
https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/assets/logos/
```

So to use SN_web_lightmode.png, our URL would look like this
```
https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/assets/logos/SN_web_lightmode.png
```
